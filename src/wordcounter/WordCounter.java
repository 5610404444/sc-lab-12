package wordcounter;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


public class WordCounter {
	private String filename;
	private HashMap<String, Integer> wordCount;
	private Reader fileReader;
	private FileWriter filewriter;
	private BufferedReader buffer;
	
	public WordCounter(String filename){
		this.filename = filename;
		wordCount = new HashMap<String, Integer>();
		
	}
	
	public void Count(){
		
		try {
			fileReader = new FileReader(filename);
			buffer = new BufferedReader(fileReader);
			String data ;
			
			
			for (data = buffer.readLine();data!=null;data = buffer.readLine()){
				String[] splitdata = data.split(" ");
					
					for (String m : splitdata){
						
						if(wordCount.containsKey(m)){
							wordCount.put(m, wordCount.get(m)+1);
							}
							
						
						else{
							wordCount.put(m, 1);
						}	
					}
			}
		} catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}finally{
			try {
				fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
	}
	
	public String getCountData(){
		
		String result = "";
		Set<Map.Entry<String, Integer>>entries = wordCount.entrySet();
		for(Map.Entry<String, Integer> entry : entries){
			String key = entry.getKey();
			int value = entry.getValue();
			result += key+"="+value+"\n\n";
		}
		
		return result;
	}
		

}
