package part2;

public class AverageScoreMain {

	public static void main(String[] args) {
		AverageHomeworkScore average = new AverageHomeworkScore("average.txt");
		average.averageHwScore("homework.txt");
		average.averageExamScore("exam.txt");
		System.out.println(average.toString());
	}

}
