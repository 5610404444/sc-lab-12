package part2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;


public class AverageHomeworkScore {
	
	
	private String filename; 
	private FileReader fileReader;
	private FileWriter filewriter;
	private BufferedReader buffer;
	private String fileread;
	
	public AverageHomeworkScore(String afilename){
		this.fileread = afilename;
	}
	
	
	
	public void averageHwScore(String filename){
		
		try {
			fileReader = new FileReader(filename);
			buffer = new BufferedReader(fileReader);
			filewriter = new FileWriter("average.txt");
			PrintWriter out = new PrintWriter(filewriter);
			out.println("- - - - Homework Scores - - - -");
			out.println("==============================");
			out.println("\tNAME\tAverage\n");
			String data;
			double sum = 0;
			double avg = 0; 
			
				for (data = buffer.readLine();data!=null;data = buffer.readLine()){
					String[] splitdata = data.split(", ");
					
					for(int i = 1 ; i<splitdata.length;i++){
						sum += Double.parseDouble(splitdata[i]);
						
					}
					int num = splitdata.length-1;
					avg = sum/num;
					sum =0;
					
					//write file
					
					out.println("\t"+splitdata[0]+"\t"+avg);
					out.flush();
					
				 }
			
		} catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}finally{
			try {
				fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		
	}
	
	public void averageExamScore(String filename){
		
		try {
			fileReader = new FileReader(filename);
			buffer = new BufferedReader(fileReader);
			
			filewriter = new FileWriter("average.txt",true);
			PrintWriter out = new PrintWriter(filewriter);
			
			out.println("________________________________");
			out.println("\n- - - - Exam Scores - - - -");
			out.println("==============================");
			out.println("\tNAME\tAverage\n");
			
			String data;
			double sum = 0;
			double avg = 0; 
				for (data = buffer.readLine();data!=null;data = buffer.readLine()){
					String[] splitdata = data.split(", ");
			
					for(int i = 1 ; i<splitdata.length;i++){
						sum += Double.parseDouble(splitdata[i]);
						
					}
					int num = splitdata.length-1;
					avg = sum/num;
					sum =0;
					
					//write file
					
					out.println("\t"+splitdata[0]+"\t"+avg);
					out.flush();
					
				 }
			
		} catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}finally{
			try {
				fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		
		
	}
	
	public String toString(){
		String collect="";
		try {
			fileReader = new FileReader(fileread);
			buffer = new BufferedReader(fileReader);
			String data;
			 
				for (data = buffer.readLine();data!=null;data = buffer.readLine()){
					
					 collect += data+"\n";
				 }
			
		} catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}finally{
			try {
				fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		return collect;
	}
	
	
	
}
