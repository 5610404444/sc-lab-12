package part2;

public class Contact {
	private String name;
	private String tel;
	
	public Contact(String aName,String aTel){
		this.name = aName;
		this.tel = aTel;
	}
	
	public String getName(){ 
		return name;
	}
	
	public String getTel(){ 
		
		return tel;
		
	}
	
	

}
