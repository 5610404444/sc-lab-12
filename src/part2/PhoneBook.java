package part2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class PhoneBook {
	
	
	private String filename; 
	private FileReader fileReader;
	private BufferedReader buffer;
	
	private ArrayList<Contact> contacinfo;
	
	
	
	
	public PhoneBook(String afilename){
		
		this.filename = afilename;
		contacinfo = new ArrayList<Contact>();
		
		
	}
	
	
	public String toString(){
		String collect="";
		try {
			fileReader = new FileReader(filename);
			buffer = new BufferedReader(fileReader);
			String data;
			 collect +="----------JAVA PHONE BOOK--------\n"+"\n\tNAME\t\tPHONE";
				for (data = buffer.readLine();data!=null;data = buffer.readLine()){
					String[] splitdata = data.split(", ");
					contacinfo.add(new Contact(splitdata[0], splitdata[1]));
					collect += "\n\n\t"+splitdata[0]+"\t\t"+splitdata[1];
				 }
			
		} catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}finally{
			try {
				fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		return collect;
	}
	
	
	
	//data in array
	public String collectingArray(){
		String info ="\n\n-----Collecting coontact in Array-----\n\n";
		for(Contact m:contacinfo){
			info += m.getName()+" : "+m.getTel()+"\n\n";
		}
		return info;
		
	}

}
